function Palabras()
{
    var texto = document.getElementById("cadena").value;
    var longitud = 0;

    for(contador = 0; contador < texto.length; contador++)
    {
        longitud = longitud + 1;
    }

    document.getElementById("longitud").innerHTML = "La longitud de esta cadena es: " + longitud;
    document.getElementById("cursivo").innerHTML = texto;
    document.getElementById("cursivo").style.fontStyle = "italic";
    document.getElementById("cursivo").style.textDecoration = "line-through";
}