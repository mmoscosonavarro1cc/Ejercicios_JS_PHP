function Respuestas(respuestasMandadas)
{
    var puntos = 0;

    if(!/hola/i.test(respuestasMandadas.texto.value))
    {
        alert("La respuesta es incorrecta");
    }
    else
    {
        document.getElementById("texto").style.display = "none";
        puntos = puntos + 5;
    }
    
    if(respuestasMandadas.correcta.checked == false && respuestasMandadas.correcta2.checked == false)
    {
        alert("La respuesta es incorrecta");
    }
    else 
    if(respuestasMandadas.correcta.checked == false || respuestasMandadas.correcta2.checked == false)
    {
        puntos = puntos + 2.5;
    }
    else
    {
        document.getElementById("check").style.display = "none";
        puntos = puntos + 5;
    }
}