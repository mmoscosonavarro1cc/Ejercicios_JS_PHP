var diccionario = ["hola", "jamon", "piscina"];
var numero = Math.round(Math.random()*2);
var palabra = diccionario[numero];
var palabraVacia = [];
var abecedario = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
var intentos = 0;
var vocales = ["a", "e", "i", "o", "u"];

console.log(palabra);

window.onload = function()
{
    var dificultades = ["FÁCIL", "MEDIO", "DÍFICIL"];
    for(contador = 0; contador < dificultades.length; contador++)
    {
        //Creamos el elemento
        var boton = document.createElement("button");
        var contenido = document.createTextNode(dificultades[contador]);
                
        boton.id = "quitar" + contador;
        boton.value = contador + 1;
        //Con este se le añade un evento
        boton.addEventListener("click", Dificultad, false);
        boton.appendChild(contenido);
        //Lo introducimos dentro del elemento que indiquemos
        document.getElementById("botones").appendChild(boton);
    }

    document.getElementById("palabraCompleta").innerHTML = palabra;
    document.getElementById("palabraCompleta").style.display = "none";
}

function Dificultad()
{
    if(this.value == 1)
    {
        intentos = 999;
    }
    else 
    if(this.value == 2)
    {
        intentos = 10;
    }
    else
    if(this.value == 3)
    {
        intentos = palabra.length;
    }

    document.getElementById("quitar0").style.display = "none";
    document.getElementById("quitar1").style.display = "none";
    document.getElementById("quitar2").style.display = "none";
    document.getElementById("quitar3").style.display = "none";
    console.log(intentos);
    CreaEspacios();
    CreaBotones();
}

function CreaEspacios()
{
    for(contador = 0; contador < palabra.length; contador++)
    {
        palabraVacia[contador] = "-";
    }
    Pistas();
    document.getElementById("palabra").innerHTML = palabraVacia.join(" ");
}



function CreaBotones()
{
    for(contador = 0; contador < abecedario.length; contador++)
    {
        //Creamos el elemento
        var boton = document.createElement("button");
        var contenido = document.createTextNode(abecedario[contador]);
                
        boton.id = "letra" + contador;
        boton.value = abecedario[contador];
        //Con este se le añade un evento
        boton.addEventListener("click", CompruebaLetra, false);
        boton.appendChild(contenido);
        //Lo introducimos dentro del elemento que indiquemos
        document.getElementById("botones").appendChild(boton);
    }
}

function CompruebaLetra()
{
    var letra = this.value;
    console.log(letra);
    for(contador = 0; contador < palabra.length; contador++)
    {
        if(palabra[contador] == letra)
        {
            palabraVacia[contador] = letra;            
        }        
    }
    intentos = intentos - 1;
    Intentos();
    document.getElementById("palabra").innerHTML = palabraVacia.join(" ");
}

function Intentos()
{
    if(intentos <= 0)
    {
        for(contador = 0; contador < abecedario.length; contador++)
        {
            document.getElementById("letra" + contador).style.display = "none";
        }
        document.getElementById("palabraCompleta").style.display = "inline";
        document.getElementById("palabra").style.display = "none";
    }
}

function Pistas()
{    
    var paca = palabra.split("");
    console.log(paca);
    console.log(vocales);
    for(contador = 0; contador < palabra.length; contador++)
    {
        for(contador2 = 0; contador2 < vocales.length;contador2++)
        {
            if(paca[contador] == vocales[contador2])
            {
                palabraVacia[contador] = vocales[contador2];
            }
        }   
    }
    console.log(palabraVacia);
}