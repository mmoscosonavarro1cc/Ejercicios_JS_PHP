var array = [];

function AñadeFinal()
{
    var numero = Math.round(Math.random()*1000);
    array.push(numero);
    document.getElementById("array").innerHTML = array.join(" ");
}

function AñadePrincipio()
{
    var numero = Math.round(Math.random()*1000);
    array.unshift(numero);
    document.getElementById("array").innerHTML = array.join(" ");
}

function BorraPrimero()
{
    array.shift();
    document.getElementById("array").innerHTML = array.join(" ");
}

function BorraFinal()
{
    array.pop();
    document.getElementById("array").innerHTML = array.join(" ");
}

function OrdenAscendente()
{
    array.sort(function(a, b) {
        return a - b;
      });
    document.getElementById("array").innerHTML = array.join(" ");
}

function OrdenDescendente()
{
    array.sort(function(a, b) {
        return a + b;
      });
    document.getElementById("array").innerHTML = array.join(" ");
}