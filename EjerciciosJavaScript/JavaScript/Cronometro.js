var horas = 00;
var minutos = 00;
var segundos = 00;

function Empezar()
{
    inicio = setInterval(Cronometro, 1000);
}

function Parar()
{
    clearInterval(inicio);
    document.getElementById("registros").innerHTML +="<p>" + horas + ":" + minutos + ":" + segundos + "</p>";
}

function Reiniciar()
{
    clearInterval(inicio);
    document.getElementById("horas").innerHTML = "00:";
    document.getElementById("minutos").innerHTML = "00";
    document.getElementById("segundos").innerHTML = ":00";
    segundos = 0;
    horas = 0;
    minutos = 0;
}

function Cronometro()
{
    segundos++;
    if(segundos < 10)
    {
        document.getElementById("segundos").innerHTML = ":0" + segundos;
    }
    else
    {
        document.getElementById("segundos").innerHTML = ":" + segundos;
    }

    if(segundos == 60)
    {
        minutos = minutos + 1;
        segundos = 0;
        document.getElementById("segundos").innerHTML = segundos;
    }
    
    if(minutos < 10)
    {
        document.getElementById("minutos").innerHTML = "0" + minutos;
    }
    else
    {
        document.getElementById("minutos").innerHTML = minutos;
    }
    if(minutos == 60)
    {
        horas = horas + 1;
        minutos = 0;
        document.getElementById("minutos").innerHTML = minutos;
    }

    if(horas < 10)
    {
        document.getElementById("horas").innerHTML = "0" + horas + ":";
    }
    else
    {
        document.getElementById("horas").innerHTML = horas + ":";
    }
    if(horas == 24)
    {
        horas = 0;
    }

    
}