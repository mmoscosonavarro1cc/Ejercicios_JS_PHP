var numero = Math.round(Math.random()*100);
console.log(numero);


function CompruebaNumero()
{
    var numero2 = this.value;
    document.getElementById("numeroElegido").innerHTML = "El número seleccionado: " + numero2;
    if(numero > numero2)
    {
        document.getElementById("respuesta").innerHTML = "El número es mayor al seleccionado.";
    }
    else
    if(numero < numero2)
    {
        document.getElementById("respuesta").innerHTML = "El número es menor al seleccionado.";
    }
    else
    {
        document.getElementById("respuesta").innerHTML = "¡Has acertado el número, enhorabuena!";
        for(contador = 0; contador <= 100; contador++)
        {
            document.getElementById(contador).style.display = "none";
        }
    }
}

function CreaBotones()
{
    for(contador = 0; contador <= 100; contador++)
    {
        //Creamos el elemento
        var boton = document.createElement("button");
        var contenido = document.createTextNode(contador);
                
        boton.id = contador;
        boton.value = contador;
        //Con este se le añade un evento
        boton.addEventListener("click", CompruebaNumero, false);
        boton.appendChild(contenido);
        //Lo introducimos dentro del elemento que indiquemos
        document.getElementById("botones").appendChild(boton);
    }

    document.getElementById("empezar").style.display = "none";
}