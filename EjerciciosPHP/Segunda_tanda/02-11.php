<?php
    $hora = $_POST['hora'];
    $minuto = $_POST['minuto'];

    $segundosTranscurridos = ($hora * 3600) + ($minuto * 60);
    $segundosHastaMedianoche = (24 * 3600) - $segundosTranscurridos;

    echo "Desde las $hora:$minuto hasta la medianoche faltan ";
    echo "$segundosHastaMedianoche segundos.";
?>