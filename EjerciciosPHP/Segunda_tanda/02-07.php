<?php
    $notas = $_REQUEST["nota1"] + $_REQUEST["nota2"] + $_REQUEST["nota3"];
    $media = $notas/3;
    echo "La media es: ", round($media, $precision = 2), "<br>";
    if($media < 5)
    {
        echo "INSUFICIENTE";
    }
    else
    if($media >= 5 && $media < 6)
    {
        echo "SUFICIENTE";
    }
    else
    if($media >= 6 && $media < 7)
    {
        echo "BIEN";
    }
    else
    if($media >= 7 && $media < 9)
    {
        echo "NOTABLE";
    }
    else
    {
        echo "SOBRESALIENTE";
    }
?>