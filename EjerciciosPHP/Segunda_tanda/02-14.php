<?php
    $numero = $_REQUEST["numero"];

    if($numero % 2 != 0)
    {
        echo "Este número es impar.";
    }
    else
    {
        echo "Este número es par.";
    }

    echo "<br>";

    if($numero % 5 != 0)
    {
        echo "Este número no es divisible entre 5.";
    }
    else
    {
        echo "Este número es divisible entre 5.";
    }
?>